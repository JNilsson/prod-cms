import React from 'react'
import { Container } from 'reactstrap'
import Helmet from 'react-helmet'
import graphql from 'graphql'

export default function Template ({ data }) => (
    <div>
      <Helmet title={`${data.jsonPostJson.title} | ${data.site.siteMetadata.title}`} />
      <Container>
        <h1 className='display-3'>{data.jsonPostJson.title}</h1>
      </Container>
      <Container dangerouslySetInnerHTML={{ __html: data.jsonPostJson.content }} />
    </div>
)


export const aboutPageQuery = graphql`
  query JsonPage($path: String!) {
    jsonPostJson (path: { eq: $path }) {
      title
      content
    }
    
    site {
      siteMetadata {
        title
      }
    }
  }
`
